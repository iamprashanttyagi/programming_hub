import BaseAPIController from './BaseAPIController';
import language_model from '../models/language'

export class language extends BaseAPIController {
    /* Controller for Language Details fetch ..  */
    fetch = async (req, res, next, db) => {
        try {
            let fetch_result = await language_model(db).fetch(req.query.id, res);
            this.handleSuccessResponse(req, res, next, fetch_result)
        } catch (err) {
            this.handleErrorResponse(res, err)
        }
    }
}
const controller = new language();
export default controller;