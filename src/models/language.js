import Sequelize from 'sequelize';
import User_current_status from './user_current_status';
export default (db) => {

    const Language = db.define('language', {
        index: {
            type: Sequelize.INTEGER,
            required: true,
        },
        name: {
            type: Sequelize.STRING,
            required: true
        },
        language_id: {
            type: Sequelize.INTEGER,
            required: true
        },
        icon: {
            type: Sequelize.TEXT,
            required: true,
        },
        reference: {
            type: Sequelize.TEXT,
            required: true,
        },
        compiler: {
            type: Sequelize.TINYINT,
            required: true,
        },
        program: {
            type: Sequelize.TINYINT,
            required: true,
        },
        course: {
            type: Sequelize.TINYINT,
            required: true,
        },
        tag: {
            type: Sequelize.STRING,
            required: true,
        },

        duplicate_language_id: {
            type: Sequelize.INTEGER,
            required: true,
        },
        bundle_name: {
            type: Sequelize.STRING,
            required: true,
        },
        created_at: {
            type: Sequelize.STRING,
            required: true,
        },
        updated_at: {
            type: Sequelize.STRING,
            required: true,
        }
    }, {
        freezeTableName: true,
    });

    Language.fetch = async function (id, res) {
        try {
            let fetch_res = await Language.findAll({
                where: {
                    language_id: id
                },
                attributes: ['name']
            })
            if (fetch_res.length !== 0) {
                try {
                    var status = await User_current_status(db).fetch(id, res);
                    return status;
                } catch (err) {
                    throw new Error(err)
                }
            } else {
                throw new Error("Language id is not available...")
            }
        } catch (err) {
            throw new Error(err.message);
        }
    };
    return Language;
};