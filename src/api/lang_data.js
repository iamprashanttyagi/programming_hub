import {
	Router
} from 'express';
import language from '../controllers/language';

export default ({
	config,
	db
}) => {
	let api = Router();
	//api for getting the details related to language
	api.get('/fetch', (req, res, next) => {
		language.fetch(req, res, next, db)
	});
	return api;
}