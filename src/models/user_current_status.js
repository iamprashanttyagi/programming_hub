import Sequelize from 'sequelize';
export default (db) => {

    const User_current_status = db.define('user_current_status', {

        user_id: {
            type: Sequelize.STRING,
            required: true
        },
        language_id: {
            type: Sequelize.INTEGER,
            required: true
        },
        language_pursuing: {
            type: Sequelize.TINYINT,
            required: true,
        },
        current_course_uri: {
            type: Sequelize.STRING,
            required: true,
        },
        current_course_sequence: {
            type: Sequelize.INTEGER,
            required: true,
        },
        current_subtopic_uri: {
            type: Sequelize.STRING,
            required: true,
        },
        current_subtopic_sequence: {
            type: Sequelize.INTEGER,
            required: true,
        },
        was_pro: {
            type: Sequelize.TINYINT,
            required: true,
        },
        course_completed: {
            type: Sequelize.TINYINT,
            required: true,
        },
        created_at: {
            type: Sequelize.STRING,
            required: true,
        },
        updated_at: {
            type: Sequelize.STRING,
            required: true,
        }
    }, {
        freezeTableName: true,
    });
    User_current_status.fetch = async function (language_id, res) {
        try {
            let fetch_res = await User_current_status.findAll({
                where: {
                    language_id: language_id
                },
                attributes: ['user_id', 'language_pursuing', 'current_course_sequence', 'current_subtopic_sequence', 'course_completed']
            })
            if (fetch_res.length != 0) {
                return fetch_res;
            } else {
                throw new Error(fetch_res)
            }
        } catch (err) {
            throw new Error(err.message);
        }
    };
    return User_current_status;
};