import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import api from './api/lang_data';
import config from './config.json';
let app = express();
app.server = http.createServer(app);

app.use(bodyParser.json({
	limit: config.bodyLimit
}));

app.use(bodyParser.urlencoded({
	extended: false
}));

// connect to db
initializeDb(db => {
	// internal middleware
	app.use(middleware({
		config,
		db
	}));
	// api router
	app.use('/user', api({
		config,
		db
	}));
	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;